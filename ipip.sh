#!/bin/bash
# Set VARs
username="****@yandex.ru"
password="XXXXXXXXXX"
folder="home-work"

localport=`shuf -i 10000-65000 -n 1`
cid=`shuf -i 10000-99999 -n 1`
intip="10.1.1.1"
tid=`shuf -i 10-99 -n 1`
# Functions
function yaread {
        	curl -s --user "$1:$2" -X PROPFIND -H "Depth: 1" https://webdav.yandex.ru/$3 | sed 's/></>\n</g' | grep "displayname" | sed 's/<d:displayname>//g' | sed 's/<\/d:displayname>//g'
}
function yacreate {
        curl -s -X MKCOL --user "$1:$2" https://webdav.yandex.ru/$3
}
function yadelete {
        curl -s -X DELETE --user "$1:$2" https://webdav.yandex.ru/$3
}
function myipport {
        stun stun.sipnet.ru -v -p $1 2>&1 | grep "MappedAddress" | sort | uniq | awk '{print $3}' | head -n1
}
function tunnelup {
	echo -e "\n`date`\nRemote addr \t$1\nRemote port \t$2\nLocal addr \t$3\nLocal port \t$4\nRemone addr tun $5\nLocal addr tun \t$6\nTun interface \tfou$7"
	modprobe fou
	ip fou add port $4 ipproto 4
	ip link add name fou$7 type ipip remote $1 local $3 encap fou encap-sport $4 encap-dport $2
	ip link set up dev fou$7
	ip addr add $6 peer $5 dev fou$7
}
function tunnelcheck {
	sleep 10
        pings=0
        until [[ $pings == 4 ]]; do
                if ping -c 1 $1 -s 0 &>/dev/null;
                        then    echo -n .; n=0
                        else    echo -n !; ((pings++))
                fi
		sleep 15
        done
}
function tunneldown {
	#echo 0 > /proc/sys/net/ipv4/ip_forward
	ip link del dev fou$1
	ip fou del port $2
}
# Trap
trap 'echo -e "\n`date` Disconnecting..." && tunneldown $tunnelid $localport; echo "IPIP tunnel disconnected!"; exit 1' 1 2 3 8 9 14 15
# Code
until [[ -n $end ]]; do
    yacreate $username $password $folder
    until [[ -n $ip ]]; do
        mydate=`date +%s`
        timeout="60"
        yareq=`yaread $username $password $folder`
	if [ `echo "$yareq" | wc -l` -ge 60 ]; then
		echo "`date` Удаление старых записей"
		yadelete $username $password $folder
		sleep 1
		yacreate $username $password $folder
	fi
        list=`echo "$yareq" | grep -v $folder | grep -v $cid | sort -r | head -n1`
        yacreate $username $password $folder/$mydate:$cid
        for l in $list; do
                if [ `echo $l | sed 's/:/ /g' | awk {'print $1'}` -ge $(($mydate-65)) ]; then
			echo $list
                        myipport=`myipport $localport`
                        yacreate $username $password $folder/$mydate:$cid:$myipport:$intip:$tid
                        timeout=$(( $timeout + `echo $l | sed 's/:/ /g' | awk {'print $1'}` - $mydate + 3 ))
                        ip=`echo $l | sed 's/:/ /g' | awk '{print $3}'`
                        port=`echo $l | sed 's/:/ /g' | awk '{print $4}'`
                        peerip=`echo $l | sed 's/:/ /g' | awk '{print $5}'`
			peerid=`echo $l | sed 's/:/ /g' | awk '{print $6}'`
			if [[ -n $peerid ]]; then tunnelid=$(($peerid*$tid)); fi
                fi
        done
        if ( [[ -z "$ip" ]] && [ "$timeout" -gt 0 ] ) ; then
                echo -n "!"
                sleep $timeout
        fi
    done
    localip=`ip route get $ip | head -n1 | sed 's|.*src ||' | cut -d' ' -f1`
    tunnelup $ip $port $localip $localport $peerip $intip $tunnelid
    ipsec restart
    tunnelcheck $peerip
    tunneldown $tunnelid $localport
    unset ip port myipport
    sleep 5
done
exit 0